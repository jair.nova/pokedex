package com.example.pokedex.ui.viewmodel

import android.app.Activity
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.domain.useCases.GetPokedexUseCase
import com.example.pokedex.domain.useCases.GetPokemonUseCase
import com.example.pokedex.domain.model.PokedexItem
import com.example.pokedex.domain.model.Pokemon
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokedexViewModel @Inject constructor(
    private val getPokedexUseCase: GetPokedexUseCase,
    private val getPokemonUseCase: GetPokemonUseCase
) : ViewModel() {

    val pokedexItem = MutableLiveData<List<PokedexItem>>()
    val pokemonItem = MutableLiveData<Pokemon>()
    val isLoading = MutableLiveData<Boolean>()

    fun listPokedex(isConecte: Boolean){
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getPokedexUseCase(isConecte)
            if(result.isNotEmpty()) {
                pokedexItem.postValue(result)
                dataPokemon(result[0].name, isConecte)
                isLoading.postValue(false)
            } else{
                isLoading.postValue(false)
            }
        }
    }

    fun dataPokemon(namePokemon: String, isConecte: Boolean){
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getPokemonUseCase(namePokemon,isConecte)
            if(result.name.isNotEmpty()){
                pokemonItem.postValue(result)
                isLoading.postValue(false)
            } else{
                isLoading.postValue(false)
            }
        }
    }
}