package com.example.pokedex.ui.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pokedex.core.InternetCheck
import com.example.pokedex.core.renderImagenView
import com.example.pokedex.databinding.ItemPokemonBinding
import com.example.pokedex.domain.model.PokedexItem
import com.example.pokedex.ui.viewmodel.PokedexViewModel

class PokedexViewHolder(view:View) : RecyclerView.ViewHolder(view){

    private val binding = ItemPokemonBinding.bind(view)

    fun bind(pokemon : PokedexItem, pokedexViewModel : PokedexViewModel){
        if(pokemon != null) {
            binding.ImagePokemon.renderImagenView(pokemon.imageUrl)
            binding.ImagePokemon.setOnClickListener{
                pokedexViewModel.dataPokemon(pokemon.name, InternetCheck.isConnectedNetwork(binding.ImagePokemon.context))
            }
        }
        // para hacer clic en todoo el item
        //itemView.setOnClickListener {  }
    }
}