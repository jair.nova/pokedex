package com.example.pokedex.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.pokedex.core.InternetCheck
import com.example.pokedex.core.renderImagenView
import com.example.pokedex.databinding.ActivityMainBinding
import com.example.pokedex.domain.model.PokedexItem
import com.example.pokedex.ui.view.adapter.PokedexAdapter
import com.example.pokedex.ui.viewmodel.PokedexViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: PokedexAdapter
    private val pokedexViewModel : PokedexViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        observePokedex()
    }

    private fun observePokedex(){

        println("Filtro - conectar "+conectec())
        pokedexViewModel.listPokedex(conectec())

        pokedexViewModel.isLoading.observe(this, Observer{
            binding.progress.isVisible = it
        })

        pokedexViewModel.pokedexItem.observe(this, Observer{
            initRecyclerview(it)
        })

        pokedexViewModel.pokemonItem.observe(this, Observer{
            println("filtro -- comensando ")
            binding.namePokemon.text = it.name
            binding.imageViewPokemon.renderImagenView(it.imageUrl)
            binding.txNoPokemon.text = it.no
            binding.txLevelPokemon.text = it.level
            binding.txTypePokemon.text = it.type
            binding.txHabilityPokemon.text = it.hability
            binding.txHeightPokemon.text = it.height
            binding.txWeightPokemon.text = it.weight
            println("filtro -- terminando ")
        })
    }
    
    private fun initRecyclerview(pokedexItem : List<PokedexItem>){
        println("filtro -- recyclerview ")
        adapter = PokedexAdapter(pokedexItem, pokedexViewModel)
        binding.recyclerPokedex.layoutManager = GridLayoutManager(this, 2)
        binding.recyclerPokedex.adapter = adapter
    }

    private fun conectec() : Boolean {
        return InternetCheck.isConnectedNetwork(this)
    }
}