package com.example.pokedex.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.R
import com.example.pokedex.domain.model.PokedexItem
import com.example.pokedex.ui.viewmodel.PokedexViewModel

class PokedexAdapter (
    private val pokedexItem : List<PokedexItem>,
    private val pokedexViewModel : PokedexViewModel
) : RecyclerView.Adapter<PokedexViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokedexViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PokedexViewHolder(layoutInflater.inflate(R.layout.item_pokemon, parent, false))
    }

    override fun onBindViewHolder(holder: PokedexViewHolder, position: Int) {
        holder.bind(pokedexItem[position], pokedexViewModel)
    }

    override fun getItemCount(): Int = pokedexItem.size
}