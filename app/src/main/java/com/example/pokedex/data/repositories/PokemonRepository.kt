package com.example.pokedex.data.repositories

import com.example.pokedex.data.dataSourse.local.dao.PokemonDao
import com.example.pokedex.data.dataSourse.local.entity.PokemonEntity
import com.example.pokedex.data.dataSourse.local.entity.toDomainDataBasePokemon
import com.example.pokedex.data.dataSourse.remote.model.PokedexModel
import com.example.pokedex.data.dataSourse.remote.service.PokemonService
import com.example.pokedex.domain.model.PokedexItem
import com.example.pokedex.domain.model.Pokemon
import com.example.pokedex.domain.model.toDomainPokedexItem
import com.example.pokedex.domain.model.toDomainPokemon
import javax.inject.Inject

class PokemonRepository @Inject constructor(
   private val servicePokemon: PokemonService,
   private val daoPokemon : PokemonDao
) {

    suspend fun getDataPokemonFromApi(namePokemon : String) : Pokemon {
        val response = servicePokemon.getDataPokemon(namePokemon)
        return response.toDomainPokemon()
    }

    suspend fun getListPokemonFromApi(pokedex : PokedexModel) : List<PokedexItem>{
        var listPokemon: List<PokedexItem> = emptyList()
            for (pokemon in pokedex.listPokemon) {
                val responsePokemon = servicePokemon.getDataPokemon(pokemon.name)
                if(!responsePokemon.name.isNullOrEmpty()){
                    insertPonkemon(responsePokemon.toDomainDataBasePokemon())
                }
                listPokemon = listPokemon + responsePokemon.toDomainPokedexItem()
            }
        return listPokemon
    }

    suspend fun insertPonkemon(pokemon : PokemonEntity){
        daoPokemon.insertAll(pokemon)
    }

    suspend fun getListPokemonFromDataBase() : List<PokedexItem>{
        var listPokemon = daoPokemon.getAllPokemon()
        return listPokemon.map { it.toDomainPokedexItem() }
    }

    suspend fun getDatePokemonFromDataBase(namePokemon : String) : Pokemon{
        return daoPokemon.getDataAnPokemon(namePokemon).toDomainPokemon()
    }
}