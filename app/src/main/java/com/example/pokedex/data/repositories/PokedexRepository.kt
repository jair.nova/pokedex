package com.example.pokedex.data.repositories

import com.example.pokedex.data.dataSourse.remote.model.PokedexModel
import com.example.pokedex.data.dataSourse.remote.service.PokedexService
import javax.inject.Inject

class PokedexRepository @Inject constructor(
    private val servicePokedex : PokedexService
){
    suspend fun getAllPokedexFromApi() : PokedexModel{
        return servicePokedex.getPokedex()
    }
}