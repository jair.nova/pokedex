package com.example.pokedex.data.dataSourse.remote.service

import com.example.pokedex.data.dataSourse.remote.network.ApiClient
import com.example.pokedex.data.dataSourse.remote.model.PokedexModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PokedexService @Inject constructor(private val api : ApiClient) {

    suspend fun getPokedex():PokedexModel{

        return withContext(Dispatchers.IO){
            val response = api.getAllPokedex(20)
            response.body() ?: PokedexModel("","","", emptyList())
        }
    }
}