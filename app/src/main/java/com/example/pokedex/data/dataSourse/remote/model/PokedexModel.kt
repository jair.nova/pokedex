package com.example.pokedex.data.dataSourse.remote.model

import com.google.gson.annotations.SerializedName

data class PokedexModel(
    @SerializedName("count") val count : String,
    @SerializedName("next") val nextUrl : String,
    @SerializedName("previous") val previousUrl : String,
    @SerializedName("results") val listPokemon : List<ListPokemon>)

data class ListPokemon (@SerializedName("name")val name : String, @SerializedName("url")val url : String)