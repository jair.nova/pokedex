package com.example.pokedex.data.dataSourse.remote.network

import com.example.pokedex.data.dataSourse.remote.model.PokedexModel
import com.example.pokedex.data.dataSourse.remote.model.PokemonModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiClient {

    @GET("api/v2/pokemon")
    suspend fun getAllPokedex(@Query("offset") name : Int) : Response<PokedexModel>

    @GET("api/v2/pokemon/{name}")
    suspend fun getDataPokemon(@Path("name") name : String ) : Response<PokemonModel>

}