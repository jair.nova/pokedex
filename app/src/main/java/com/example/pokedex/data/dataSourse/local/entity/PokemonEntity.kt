package com.example.pokedex.data.dataSourse.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.pokedex.data.dataSourse.remote.model.PokemonModel
import com.example.pokedex.domain.model.Pokemon

@Entity(tableName = "pokemon")
data class PokemonEntity(
    @PrimaryKey
    @ColumnInfo(name = "id") val id : Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "image_url_small") val imageUrlSmall : String,
    @ColumnInfo(name = "image_url_Big") val imageUrlBig : String,
    @ColumnInfo(name = "level") val level : String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "hability") val hability : String,
    @ColumnInfo(name = "height") val height : String,
    @ColumnInfo(name = "weigth") val weight: String
)

fun PokemonModel.toDomainDataBasePokemon() = PokemonEntity(id = id.toInt(), name = name, imageUrlSmall =  sprites.frontDefaultLogo,
    imageUrlBig = sprites.other.officialArtwork.frontDefaultBig, level = baseExperience, type = types[0].type.name,
    hability = abilities[0].ability.name, height = height, weight = weight)
