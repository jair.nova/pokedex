package com.example.pokedex.data.dataSourse.remote.model

import com.google.gson.annotations.SerializedName

data class PokemonModel(
    @SerializedName("abilities") val abilities : List<Abilities>,
    @SerializedName("base_experience") val baseExperience : String,
    @SerializedName("height") val height : String,
    @SerializedName("id") val id : String,
    @SerializedName("name") val name : String,
    @SerializedName("sprites") val sprites : Sprites,
    @SerializedName("types") val types : List<Types>,
    @SerializedName("weight") val weight : String
    )

data class Abilities(
    val ability : Ability,
    val is_hidden : String,
    val slot : String
    )

data class Ability(val name : String, val url : String)

data class Sprites(
    @SerializedName("front_default") val frontDefaultLogo : String,
    @SerializedName("other") val other: Other,
    @SerializedName("front_shiny") val frontShinyLogo : String
    )

data class Other(@SerializedName("official-artwork") val officialArtwork : OfficialArtwork)

data class OfficialArtwork(@SerializedName("front_default") val frontDefaultBig : String)

data class Types(val slot : String, val type: Type)

data class Type(val name : String, val url : String)