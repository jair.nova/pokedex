package com.example.pokedex.data.dataSourse.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pokedex.data.dataSourse.local.entity.PokemonEntity

@Dao
interface PokemonDao {

    @Query("select * from pokemon")
    suspend fun getAllPokemon() : List<PokemonEntity>

    @Query("select * from pokemon where name = :namePokemon")
    suspend fun getDataAnPokemon(namePokemon : String) : PokemonEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pokemon:PokemonEntity)

}