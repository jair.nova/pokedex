package com.example.pokedex.data.dataSourse.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.pokedex.data.dataSourse.local.dao.PokemonDao
import com.example.pokedex.data.dataSourse.local.entity.PokemonEntity

@Database(entities = [PokemonEntity::class], version = 1)
abstract class PokedexDataBase : RoomDatabase(){
    abstract fun getPokemonDao():PokemonDao
}