package com.example.pokedex.data.dataSourse.remote.service

import com.example.pokedex.data.dataSourse.remote.model.*
import com.example.pokedex.data.dataSourse.remote.network.ApiClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PokemonService @Inject constructor(private val api : ApiClient) {

    suspend fun getDataPokemon(namePokemon : String) : PokemonModel{

        return withContext(Dispatchers.IO){
            val response = api.getDataPokemon(namePokemon)
            response.body() ?: PokemonModel(listOf(Abilities(Ability("",""),"","")),
                "","","", "", Sprites("", Other(OfficialArtwork("")),
                    ""), listOf(Types("", Type("",""))),"")
        }
    }
}