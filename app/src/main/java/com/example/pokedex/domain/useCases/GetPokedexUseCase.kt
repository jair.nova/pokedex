package com.example.pokedex.domain.useCases

import com.example.pokedex.data.repositories.PokedexRepository
import com.example.pokedex.data.repositories.PokemonRepository
import com.example.pokedex.domain.model.PokedexItem
import javax.inject.Inject

class GetPokedexUseCase @Inject constructor(
    private val repositoryPokedex: PokedexRepository,
    private val repositoryPokemon: PokemonRepository
) {

    suspend operator fun invoke(isConecte : Boolean): List<PokedexItem> {

        return if(isConecte) {
            val pokedex = repositoryPokedex.getAllPokedexFromApi()
            if(pokedex.listPokemon.isEmpty()){
                repositoryPokemon.getListPokemonFromDataBase()
            }else{
                repositoryPokemon.getListPokemonFromApi(pokedex)
            }
        } else{
            repositoryPokemon.getListPokemonFromDataBase()
        }

    }
}