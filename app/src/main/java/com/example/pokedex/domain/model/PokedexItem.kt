package com.example.pokedex.domain.model

import com.example.pokedex.data.dataSourse.local.entity.PokemonEntity
import com.example.pokedex.data.dataSourse.remote.model.PokemonModel

data class PokedexItem (val name : String, val imageUrl : String)

fun PokemonModel.toDomainPokedexItem() : PokedexItem = PokedexItem(name =  name, imageUrl = sprites.frontDefaultLogo)
fun PokemonEntity.toDomainPokedexItem() = PokedexItem(name = name, imageUrl = imageUrlSmall)