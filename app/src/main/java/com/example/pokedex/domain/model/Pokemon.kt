package com.example.pokedex.domain.model

import com.example.pokedex.data.dataSourse.local.entity.PokemonEntity
import com.example.pokedex.data.dataSourse.remote.model.PokemonModel

data class Pokemon(
    val name: String,
    val imageUrl: String,
    val no: String,
    val level: String,
    val type: String,
    val hability: String,
    val height: String,
    val weight: String
    )

fun PokemonModel.toDomainPokemon() = Pokemon(
    name = name, imageUrl = sprites.other.officialArtwork.frontDefaultBig,
    no = id, level = baseExperience, type = types[0].type.name, hability = abilities[0].ability.name,
    height = height, weight = weight)

fun PokemonEntity.toDomainPokemon() = Pokemon(
    name = name, imageUrl = imageUrlBig, no = id.toString(),
    level = level, type = type, hability = hability, height = height, weight = weight)