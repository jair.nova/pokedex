package com.example.pokedex.domain.useCases

import com.example.pokedex.data.repositories.PokedexRepository
import com.example.pokedex.data.repositories.PokemonRepository
import com.example.pokedex.domain.model.Pokemon
import javax.inject.Inject

class GetPokemonUseCase @Inject constructor(
    private val repository: PokemonRepository){

    suspend operator fun invoke(namePokemon: String,isConecte: Boolean): Pokemon {

        return if(isConecte){
            val response = repository.getDataPokemonFromApi(namePokemon)
            if (response.name.isNotEmpty()) response else repository.getDatePokemonFromDataBase(namePokemon)
        } else{
            repository.getDatePokemonFromDataBase(namePokemon)
        }
    }
}