package com.example.pokedex.core

import android.app.Activity
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.renderImagenView(imageUrl : String){

    if(!imageUrl.isNullOrEmpty()) {
        Glide.with(this.context).load(imageUrl).into(this)
    }
}