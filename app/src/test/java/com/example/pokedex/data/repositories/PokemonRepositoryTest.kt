package com.example.pokedex.data.repositories

import com.example.pokedex.data.dataSourse.local.dao.PokemonDao
import com.example.pokedex.data.dataSourse.local.entity.PokemonEntity
import com.example.pokedex.data.dataSourse.local.entity.toDomainDataBasePokemon
import com.example.pokedex.data.dataSourse.remote.model.*
import com.example.pokedex.data.dataSourse.remote.service.PokemonService
import com.example.pokedex.domain.model.PokedexItem
import com.example.pokedex.domain.model.toDomainPokedexItem
import com.example.pokedex.domain.model.toDomainPokemon
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class PokemonRepositoryTest{

    @Mock
    private lateinit var service: PokemonService

    @Mock
    private lateinit var dao: PokemonDao

    private lateinit var pokemonRepository: PokemonRepository

    @Before
    fun onBefore(){
        pokemonRepository = PokemonRepository(service,dao)
    }

    private val pokemonModel = PokemonModel(
        listOf(Abilities(Ability("keen-eye","https://pokeapi.co/api/v2/ability/51/"),"false","1")),"52","3","21", "spearow",
        Sprites("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/21.png",
            Other(OfficialArtwork("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/21.png")),
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/shiny/21.png"),
        listOf(Types("1", Type("normal","https://pokeapi.co/api/v2/type/1/"))),"20")

    private val pokemon = PokemonModel(
        listOf(Abilities(Ability("",""),"","")),"","","", "",
        Sprites("", Other(OfficialArtwork("")), ""),
        listOf(Types("", Type("",""))),"")

    private val pokemonItem = PokedexItem("spearow","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/21.png")

    @Test
    fun `when the api get all the data a pokemon`() = runBlocking {
        //Given
        Mockito.`when`(service.getDataPokemon(pokemonModel.name)).thenReturn(pokemonModel)

        //When
        val response = pokemonRepository.getDataPokemonFromApi(pokemonModel.name)

        //Then
        assert(response == pokemonModel.toDomainPokemon())
    }

    @Test
    fun `when the api doesnt get all the data a pokemon` () = runBlocking {
        //Given

        Mockito.`when`(service.getDataPokemon(pokemonModel.name)).thenReturn(pokemon)

        //When
        val response = pokemonRepository.getDataPokemonFromApi(pokemonModel.name)

        //Then
        assert(response == pokemon.toDomainPokemon())
    }

    @Test
    fun `when the API returns a list from pokemon`() = runBlocking {
        //Given
        val pokedexModel = PokedexModel("1154","https://pokeapi.co/api/v2/pokemon?offset=40&limit=20",
            "https://pokeapi.co/api/v2/pokemon?offset=0&limit=20",listOf(ListPokemon("spearow","https://pokeapi.co/api/v2/pokemon/21/")))
        Mockito.`when`(service.getDataPokemon(pokedexModel.listPokemon[0].name)).thenReturn(pokemonModel)

        //When
        val response  = pokemonRepository.getListPokemonFromApi(pokedexModel)

        //Then
        assert(response ==  listOf(pokemonModel.toDomainPokedexItem()))
    }

    @Test
    fun `when the api doesnt returns a list from pokemon`() = runBlocking {
        //Given
        val pokedexModel = PokedexModel("1154","https://pokeapi.co/api/v2/pokemon?offset=40&limit=20",
            "https://pokeapi.co/api/v2/pokemon?offset=0&limit=20",listOf(ListPokemon("spearow","https://pokeapi.co/api/v2/pokemon/21/")))
        Mockito.`when`(service.getDataPokemon(pokedexModel.listPokemon[0].name)).thenReturn(pokemon)

        //When
        val response  = pokemonRepository.getListPokemonFromApi(pokedexModel)
        println("Filtro -- respuesta response: $response")
        //Then
        assert(response == listOf(PokedexItem("","")))
    }

    @Test
    fun `when the BD returns a list from pokemon`() = runBlocking {
        //Given
        val listPokemon = listOf(pokemonModel.toDomainDataBasePokemon())
        Mockito.`when`(dao.getAllPokemon()).thenReturn(listPokemon)

        //When
        val response = pokemonRepository.getListPokemonFromDataBase()

        //Then
        assert(response == listPokemon.map { it.toDomainPokedexItem() })
    }

    @Test
    fun `when the BD doesnt returns a list from pokemon`() = runBlocking {
        //Given
        val listPokemon = emptyList<PokemonEntity>()
        Mockito.`when`(dao.getAllPokemon()).thenReturn(listPokemon)

        //When
        val response = pokemonRepository.getListPokemonFromDataBase()

        //Then
        assert(response == listPokemon.map { it.toDomainPokedexItem() })
    }

    @Test
    fun `when the BD get all the data a pokemon`() = runBlocking {
        //Given
        val pokemonEntity = pokemonModel.toDomainDataBasePokemon()
        Mockito.`when`(dao.getDataAnPokemon(pokemonModel.name)).thenReturn(pokemonEntity)

        //When
        val response = pokemonRepository.getDatePokemonFromDataBase(pokemonModel.name)

        //Then
        assert(response == pokemonEntity.toDomainPokemon())
    }

    @Test
    fun `when the BD doesnt get all the data a pokemon`() = runBlocking {
        //Given
        val pokemonEntity = PokemonEntity(-1, "", "",
            "", "", "", "", "", "")
        Mockito.`when`(dao.getDataAnPokemon(pokemonModel.name)).thenReturn(pokemonEntity)

        //When
        val response = pokemonRepository.getDatePokemonFromDataBase(pokemonModel.name)

        //Then
        assert(response == pokemonEntity.toDomainPokemon())
    }
}