package com.example.pokedex.data.service

import com.example.pokedex.data.dataSourse.remote.model.*
import com.example.pokedex.data.dataSourse.remote.service.PokedexService
import com.example.pokedex.data.repositories.PokedexRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Assert.assertEquals

@RunWith(MockitoJUnitRunner::class)
class PokedexRepositoryTest{

    @Mock
    private lateinit var servicePokedex : PokedexService

    lateinit var pokedexRepository: PokedexRepository

    @Before
    fun onBefore(){
        pokedexRepository = PokedexRepository(servicePokedex)
    }

    @Test
    fun `when the API returns a pokedex`() = runBlocking {

        //Given
        val pokedexModel = PokedexModel("1154","https://pokeapi.co/api/v2/pokemon?offset=40&limit=20",
            "https://pokeapi.co/api/v2/pokemon?offset=0&limit=20",listOf(ListPokemon("spearow","https://pokeapi.co/api/v2/pokemon/21/")))

        Mockito.`when`(servicePokedex.getPokedex()).thenReturn(pokedexModel)

        //When
        val response = pokedexRepository.getAllPokedexFromApi()

        //listOf(PokedexItem("spearow","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/21.png"))
        //Then
        assertEquals(pokedexModel, response)
    }

    @Test
    fun `when the api doesnt returns a pokedex`() = runBlocking {
        //Given
        val pokedexModel = PokedexModel("","","", emptyList())
        Mockito.`when`(servicePokedex.getPokedex()).thenReturn(pokedexModel)
        //When
        val listPokemonsResponse = pokedexRepository.getAllPokedexFromApi()
        //Then
        assertEquals(pokedexModel, listPokemonsResponse)
    }

}