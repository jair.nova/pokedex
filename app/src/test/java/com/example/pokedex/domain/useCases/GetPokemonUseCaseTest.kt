package com.example.pokedex.domain.useCases

import com.example.pokedex.data.repositories.PokemonRepository
import com.example.pokedex.domain.model.Pokemon
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class GetPokemonUseCaseTest {

    @Mock
    private lateinit var repository: PokemonRepository

    private lateinit var getPokemonUseCase: GetPokemonUseCase

    @Before
    fun onBefore() {
        getPokemonUseCase = GetPokemonUseCase(repository)
    }

    private val pokemon = Pokemon("spearow","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/21.png",
    "21","52","normal", "keen-eye","3","20")


    @Test
    fun `get from Api the data pokemon`() = runBlocking{

        //Given
        Mockito.`when`(repository.getDataPokemonFromApi(pokemon.name)).thenReturn(pokemon)

        //When
        var response = getPokemonUseCase(pokemon.name,true)

        //Then
        assert(response == pokemon)
    }

    @Test
    fun `get the data pokemon without internet connection` () = runBlocking {

        //Given
        Mockito.`when`(repository.getDatePokemonFromDataBase(pokemon.name)).thenReturn(pokemon)

        //When
        var response = getPokemonUseCase(pokemon.name,false)

        //Then
        assert(response == pokemon)
    }
}