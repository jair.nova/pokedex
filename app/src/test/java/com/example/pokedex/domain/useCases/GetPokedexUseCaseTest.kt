package com.example.pokedex.domain.useCases

import com.example.pokedex.data.dataSourse.remote.model.*
import com.example.pokedex.data.repositories.PokedexRepository
import com.example.pokedex.data.repositories.PokemonRepository
import com.example.pokedex.domain.model.PokedexItem
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class GetPokedexUseCaseTest{

    @Mock
    private lateinit var pokedexRepository: PokedexRepository

    @Mock
    private lateinit var pokemonRepository: PokemonRepository

    private lateinit var getPokedexUseCase: GetPokedexUseCase

    @Before
    fun onBefore(){
        getPokedexUseCase = GetPokedexUseCase(pokedexRepository,pokemonRepository)
    }

    private val pokedexModel = PokedexModel("1154","https://pokeapi.co/api/v2/pokemon?offset=20&limit=20",
        "null", listOf(
            ListPokemon("bulbasaur", "https://pokeapi.co/api/v2/pokemon/1/"),
            ListPokemon("ivysaur", "https://pokeapi.co/api/v2/pokemon/2/")))

    private val pokedexList = listOf(
        PokedexItem(
            "spearow",
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/21.png"
        ),
        PokedexItem(
            "fearow",
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/22.png"
        )
    )

    private val pokedexEmpity = PokedexModel("","","", emptyList())

    @Test
    fun `get pokedex list from Api`() = runBlocking{

        //Given
        Mockito.`when`(pokedexRepository.getAllPokedexFromApi()).thenReturn(pokedexModel)
        Mockito.`when`(pokemonRepository.getListPokemonFromApi(pokedexModel)).thenReturn(pokedexList)

        //When
        val response = getPokedexUseCase(true)
        println("Filtro -- respuesta: $response")
        //Then
        assert(response == pokedexList)
    }

    @Test
    fun `get pokedex list without internet connection`() = runBlocking(){
        //Given
        Mockito.`when`(pokemonRepository.getListPokemonFromDataBase()).thenReturn(pokedexList)

        //When
        val response = getPokedexUseCase(false)

        //Then
        assert(response == pokedexList)
    }

    @Test
    fun `get empty pokedex list from Api`() = runBlocking{
        //Given
        Mockito.`when`(pokedexRepository.getAllPokedexFromApi()).thenReturn(pokedexEmpity)
        Mockito.`when`(pokemonRepository.getListPokemonFromDataBase()).thenReturn(pokedexList)

        //When
        val response = getPokedexUseCase(true)

        //Then
        assert(response == pokedexList)
    }
}