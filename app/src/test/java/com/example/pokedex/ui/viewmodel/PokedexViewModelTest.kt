package com.example.pokedex.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.pokedex.domain.useCases.GetPokedexUseCase
import com.example.pokedex.domain.useCases.GetPokemonUseCase
import com.example.pokedex.domain.model.PokedexItem
import com.example.pokedex.domain.model.Pokemon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class PokedexViewModelTest {

    @Mock
    private lateinit var getPokedexUseCase: GetPokedexUseCase

    @Mock
    private lateinit var getPokemonUseCase: GetPokemonUseCase

    private lateinit var pokedexViewModel: PokedexViewModel

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun onBefore() {
        pokedexViewModel = PokedexViewModel(getPokedexUseCase, getPokemonUseCase)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }

    private val pokemon = Pokemon("spearow","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/21.png",
        "21","52", "flying", "keen-eye","3","20")

    private val isConecte = true

    @Test
    fun `when viewmodel is first created get list of pokemon`() = runTest {

        //Given
        val pokedexList = listOf(
            PokedexItem(
                "spearow",
                "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/21.png"
            ),
            PokedexItem(
                "fearow",
                "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/22.png"
            )
        )

        Mockito.`when`(getPokedexUseCase(isConecte)).thenReturn(pokedexList)

        //When
        pokedexViewModel.listPokedex(true)

        //Then
        assert(pokedexViewModel.pokedexItem.value == pokedexList)
    }

    @Test
    fun `when call dataPokemon with the name return the pokemon data`() = runTest {

        //Given
        Mockito.`when`(getPokemonUseCase("spearow", isConecte)).thenReturn(pokemon)

        //When
        pokedexViewModel.dataPokemon("spearow",isConecte)

        //Then
        assert(pokedexViewModel.pokemonItem.value == pokemon)
    }

    @Test
    fun `when PokemonUseCase  return null keep the last value`() = runTest {

        //Given
        pokedexViewModel.pokemonItem.value = pokemon
        Mockito.`when`(getPokemonUseCase("fearow", isConecte)).thenReturn(null)

        //When
        pokedexViewModel.dataPokemon("fearow", isConecte)

        //Then
        assert(pokedexViewModel.pokemonItem.value == pokemon)
    }
}